---
layout: default
author: Michal Kasarda
title: Introduction
nav_order: 0
---

# Introduction

In the field of probabilistic robotics and computer vision, filtering techniques play a crucial role in state estimation and noise reduction. 
This lecture notes covers three topics: the Discrete Bayes Filter, the Particle Filter, and Robust Loss Functions with RANSAC.

### Discrete Bayes Filter

The Discrete Bayes Filter is a probabilistic method for estimating the state of a system given a sequence of observations. It operates under the Markov assumption, where the current state depends only on the previous state and the current observation. [Learn more in the Discrete Bayes Filter notes](./bayes_filter/Bayes_filter.html).

### The Particle Filter

The Particle Filter extends the principles of the Discrete Bayes Filter to continuous state spaces. By representing the posterior distribution with a set of weighted particles, this filter can approximate complex distributions and handle non-linear and non-Gaussian systems effectively. [Learn more in the Particle Filter notes](./particle_filter/Particle_filter.html).

### Comparison of methods

So far we explored four aproaches of state estimation: Kalman filter, Factorgraph, Particle Filter and Discrete Bayes filter. [You can learn more about advantages and disatvantages of each here](./Comparison_of_methods/Comparison_of_methods.html).


### Robust loss functions and RANSAC

Robust loss functions are essential in handling outliers in data fitting problems, in our case ICP alghorithm. The RANSAC (Random Sample Consensus) algorithm is a robust estimation method that iteratively selects a subset of the data to fit a model, aiming to maximize the number of inliers. [Learn more about robust loss functions and RANSAC here](./Robust_loss_functions/Robust_loss_functions.html).

---

**Author: Michal Kasarda**